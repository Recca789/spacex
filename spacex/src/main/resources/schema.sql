drop table if exists tourist;

create table tourist
(
   id integer not null,
   name varchar(255) not null,
   surnmae varchar(255) not null,
   sex varchar(20),
   notes varchar(255),
   birth_date date,
   primary key(id)
);

drop table if exists flight;

create table  flight
(
  id integer not null,
  start_date datetime not null,
  end_date datetime not null,
  seats integer,
  tourists integer,
  ticket_price integer,
  primary key(id)
);

drop table if exists orders;

create table orders (
  tourist_id integer,
  flight_id integer,
  foreign key (tourist_id) references tourist(id),
  foreign key (flight_id) references flight(id),
);