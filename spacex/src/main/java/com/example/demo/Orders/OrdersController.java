package com.example.demo.Orders;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class OrdersController {

    private final OrdersService service;

    public OrdersController(OrdersService service) {
        this.service = service;
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getAllFlights() {
        List<Order> orders = service.getOrders();
        return ResponseEntity.ok(orders);
    }

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getFlight(@PathVariable long id) {
        Order order = service.getOrderForTourist(id);
        return ResponseEntity.ok(order);
    }

    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity addFlight(@PathVariable long tourist_id, @PathVariable long flight_id) {
        int response = service.addOrder(tourist_id, flight_id);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/flights/{tourist_id}")
    public @ResponseBody
    ResponseEntity deleteFlight(@PathVariable long tourist_id) {
        int response = service.deleteOrdeByTouristId(tourist_id);
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ResponseEntity handleException(IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
