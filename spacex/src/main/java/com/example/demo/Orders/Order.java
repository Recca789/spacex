package com.example.demo.Orders;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {

    private long touristId;
    private long flightId;
}
