package com.example.demo.Orders;

import com.example.demo.Flight.Flight;
import com.example.demo.Flight.FlightMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrdersJdbcRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Order findByTouristId(long id) {
        return jdbcTemplate.queryForObject("select * from orders where tourist_id =?", new Object[] {id}, new BeanPropertyRowMapper<>(Order.class));
    }

    public List<Order> findAll() {
        return jdbcTemplate.query("select * from orders", new OrdersMapper());
    }

    public int deleteByTouristId(long tourist_id) {
        return jdbcTemplate.update("delete from orders where tourist_id=?", new Object[] {tourist_id});
    }

    public int insert(long flight_id, long tourist_id) {
        return jdbcTemplate.update("insert into orders(tourist_id, flight_id) " + "values(?,  ?)",
                new Object[] { tourist_id, flight_id});
    }
}
