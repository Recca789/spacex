package com.example.demo.Orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersService {

    @Autowired
    OrdersJdbcRepository repository;

    public List<Order> getOrders() {
        return repository.findAll();
    }

    public Order getOrderForTourist(long id) {
        return repository.findByTouristId(id);
    }

    public int addOrder(long tourist_id, long flight_id){
        return repository.insert(tourist_id, flight_id);
    }

    public int deleteOrdeByTouristId(long tourist_id) {
        return repository.deleteByTouristId(tourist_id);
    }
}
