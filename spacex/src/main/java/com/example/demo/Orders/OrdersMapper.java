package com.example.demo.Orders;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrdersMapper implements RowMapper<Order> {
    @Override
    public Order mapRow(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order();
        order.setFlightId(resultSet.getLong("flight_id"));
        order.setTouristId(resultSet.getLong("tourist_id"));
        return order;
    }
}
