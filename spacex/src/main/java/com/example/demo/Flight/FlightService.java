package com.example.demo.Flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightService {

    @Autowired
    FlightJdbcRepository repository;

    public List<Flight>  getFlights(){
        return repository.findAll();
    }

    public Flight getFlight(long id) {
        return repository.findById(id);
    }

    public int addFlight(Flight flight) {
        return repository.insert(flight);
    }

    public int updateFlight(Flight flight) {
        return repository.update(flight);
    }

    public int deleteFlight(long id) {
        return repository.deleteById(id);
    }
}
