package com.example.demo.Flight;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class FlightController {

    private final FlightService service;

    public FlightController(FlightService service) {
        this.service = service;
    }

    @RequestMapping(value = "/flights", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getAllFlights(){
        List<Flight> flights =  service.getFlights();
        return ResponseEntity.ok(flights);
    }

    @RequestMapping(value = "/flights/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getFlight(@PathVariable long id){
        Flight flight =  service.getFlight(id);
        return ResponseEntity.ok(flight);
    }

    @RequestMapping(value= "/flights", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity addFlight(@RequestBody Flight newFlight) {
        int response = service.addFlight(newFlight);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/flights/{id}")
    public @ResponseBody
    ResponseEntity deleteFlight(@PathVariable long id) {
        int response = service.deleteFlight(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/flights")
    public @ResponseBody
    ResponseEntity updateFlight(@PathVariable Flight flight) {
        int response = service.updateFlight(flight);
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ResponseEntity handleException(IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
