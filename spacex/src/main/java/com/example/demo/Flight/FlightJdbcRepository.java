package com.example.demo.Flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FlightJdbcRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Flight findById(long id) {
        return jdbcTemplate.queryForObject("select * from flight where id =?", new Object[] {id}, new BeanPropertyRowMapper<>(Flight.class));
    }

    public List<Flight> findAll() {
        return jdbcTemplate.query("select * from flight", new FlightMapper());
    }

    public int deleteById(long id) {
        return jdbcTemplate.update("delete from flight where id=?", new Object[] {id});
    }

    public int insert(Flight flight) {
        return jdbcTemplate.update("insert into flight(id, start_date, end_date) " + "values(?,  ?, ?)",
                new Object[] { flight.getId(), flight.getStartDate(), flight.getEndDate() });
    }

    public int update(Flight flight) {
        return jdbcTemplate.update("update flight " + " set start_date = ?, end_date = ? " + " where id = ?",
                new Object[] { flight.getStartDate(), flight.getEndDate(), flight.getId() });
    }
}
