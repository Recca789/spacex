package com.example.demo.Flight;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FlightMapper implements RowMapper<Flight> {
    @Override
    public Flight mapRow(ResultSet resultSet, int i) throws SQLException {
        Flight flight = new Flight();
        flight.setId(resultSet.getLong("id"));
        flight.setStartDate(resultSet.getDate("start_date"));
        flight.setEndDate(resultSet.getDate("end_date"));
        flight.setSeats(resultSet.getInt("seats"));
        flight.setTourists(resultSet.getInt("tourists"));
        flight.setTicketPrice(resultSet.getInt("ticket_price"));
        return flight;
    }
}
