package com.example.demo.Flight;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
public class Flight {

    private long id;
    private Date startDate;
    private Date endDate;
    private Integer seats;
    private Integer tourists;
    private Integer ticketPrice;

}
