package com.example.demo.Tourist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class TouristService {

    @Autowired
    TouristJdbcRepository repository;

    public List<Tourist> getTourists () {
        return repository.findAll();
    }

    public Tourist getTourist(long id) {
        return repository.findById(id);
    }

    public int insertTourist(Tourist newTourist) {
        return repository.insert(newTourist);
    }

    public int deleteTourist(long id) {
        return repository.deleteById(id);
    }

    public int updateTourist(Tourist tourist) {
        return repository.update(tourist);
    }
}
