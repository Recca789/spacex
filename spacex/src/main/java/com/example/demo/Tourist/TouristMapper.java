package com.example.demo.Tourist;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TouristMapper implements RowMapper<Tourist> {
@Override
public Tourist mapRow(ResultSet rs, int rowNum) throws SQLException {
        Tourist tourist = new Tourist();
        tourist.setId(rs.getLong("id"));
        tourist.setName(rs.getString("name"));
        tourist.setSex(rs.getString("sex"));
        tourist.setNotes(rs.getString("notes"));
        tourist.setBirthDate(rs.getDate("birth_date"));
        return tourist;
        }
}
