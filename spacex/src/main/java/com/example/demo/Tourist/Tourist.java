package com.example.demo.Tourist;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
public class Tourist {

    private long id;
    private String name;
    private String surname;
    private String sex;
    private String notes;
    private Date birthDate;

}
