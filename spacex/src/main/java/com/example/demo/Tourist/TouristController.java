package com.example.demo.Tourist;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class TouristController {

    private final TouristService service;

    public TouristController(TouristService service) {
        this.service = service;
    }

    @RequestMapping(value = "/tourists", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getAllTourist() {
        List<Tourist> tourists = service.getTourists();
        return ResponseEntity.ok(tourists);
    }

    @RequestMapping(value = "/tourists/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getTourist(@PathVariable long id) {
        Tourist tourists = service.getTourist(id);
        return ResponseEntity.ok(tourists);
    }

    @RequestMapping(value = "/tourists", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity addTourist(@RequestBody Tourist newTourist) {
        int response = service.insertTourist(newTourist);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/tourists/{id}")
    public @ResponseBody
    ResponseEntity deleteTourist(@PathVariable long id) {
        int response = service.deleteTourist(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/tourists")
    public @ResponseBody
    ResponseEntity updateTourist(@PathVariable Tourist tourist) {
        int response = service.updateTourist(tourist);
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ResponseEntity handleException(IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
