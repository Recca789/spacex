package com.example.demo.Tourist;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class TouristJdbcRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Tourist findById(long id) {
        return jdbcTemplate.queryForObject("select * from tourist where id =?", new Object[] {id}, new BeanPropertyRowMapper<>(Tourist.class));
    }

    public List<Tourist> findAll() {
        return jdbcTemplate.query("select * from tourist", new TouristMapper());
    }

    public int deleteById(long id) {
        return jdbcTemplate.update("delete from tourist where id=?", new Object[] {id});
    }

    public int insert(Tourist tourisit) {
        return jdbcTemplate.update("insert into tourist (id, name, surname) " + "values(?,  ?, ?)",
                new Object[] { tourisit.getId(), tourisit.getName(), tourisit.getSurname() });
    }

    public int update(Tourist tourisit) {
        return jdbcTemplate.update("update tourist " + " set name = ?, surname = ? " + " where id = ?",
                new Object[] { tourisit.getName(), tourisit.getSurname(), tourisit.getId() });
    }
}
