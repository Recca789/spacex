<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Spring boot TEST</title>
    <link rel="stylesheet" href="resources\css\materialize.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="resources\js\jquery-3.3.1.min.js"></script>
</head>
<body>
<nav class="grey darken-4" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">SpaceX</a>
        <ul class="right">
            <li><a href="index.jsp"><i class="material-icons">people_outline</i></a></li>
            <li><a href="flights.jsp"><i class="material-icons">airplanemode_active</i></a></li>
            <li><a href="#"><i class="material-icons">perm_contact_calendar</i></a></li>
        </ul>
    </div>
</nav>

<div class="section no-pad-bot" id="index-banner">
    <div class="container">

        <h1 class="header center orange-text">Tourists</h1>

    </div>
</div>

<div class="section no-pad-bot">
    <div class="row center" style="padding-left: 10px; padding-right: 10px;">
        <div class="col s12">
            <table id="records_table" class="centered">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Sex</th>
                    <th>Country</th>
                    <th>Notes</th>
                    <th>Birth date</th>
                </tr>
                </thead>

                <tbody>

                </tbody>

            </table>
        </div>
    </div>
</div>

<div class="section no-pad-bot" id="index-banner-2">
    <div class="container">
        <br><br>
        <div class="row center">
            <button class="waves-effect waves-light btn modal-trigger" data-target="modal1">Add User</button>
        </div>
        <br><br>
    </div>
</div>

<footer class="page-footer orange">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">

            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by Pawel Jozwik
        </div>
    </div>
</footer>

<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
        <h4>Add an Image</h4>

        <div class="row">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="user_name" type="text" class="validate">
                        <label for="user_name">Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="user_surname" type="text" class="validate">
                        <label for="user_surname">Surename</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select id="user_sex">
                            <option value="" disabled selected>Choose your sex</option>
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                        </select>
                        <label>Sex</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select>
                            <option value="" disabled selected>Choose your country</option>
                            <option value="1">Poland</option>
                            <option value="2">USA</option>
                        </select>
                        <label>Country</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="user_notes" type="text" class="materialize-textarea validate"></textarea>
                        <label for="user_notes">Notes</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="user_birthdate" class="datepicker">
                        <label for="user_birthdate">Birthdate</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a class=" modal-action modal-close waves-effect waves-green btn-flat">Submit</a>
    </div>
</div>


<!--  Scripts-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="resources\js\index.js"></script>
<script type="text/javascript" src="resources\js\materialize.min.js"></script>

</body>
</html>