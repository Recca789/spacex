$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    appendTable();
    $('.modal').modal();
    $('.datepicker').datepicker();
    $('select').formSelect();
});

function appendTable() {

    $.ajax({
        type: "GET",
        url: "/tourists",
        data: "",
        success: function(data){
            $(function() {
                $.each(data, function(i, item) {
                    var $tr = $('<tr>').append(
                        $('<td>').text(item.id),
                        $('<td>').text(item.name),
                        $('<td>').text(item.surname),
                        $('<td>').text(item.sex),
                        $('<td>').text(item.country),
                        $('<td>').text(item.notes),
                        $('<td>').text(item.birthDate),
                        $('<td>').html('<a class="waves-effect waves-light btn">button</a>')
                    ).appendTo('#records_table');
                });
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(''+jqXHR.responseText+jqXHR.status);
        }
    });
}